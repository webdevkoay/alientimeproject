package AlienTime;

import java.util.Arrays;
import java.util.Scanner;

public class AlienTime implements TimeInterface {

    protected static final int BASE_TIME_YEAR = 2804;

    protected static final int BASE_TIME_MONTH = 18;

    protected static final int BASE_TIME_DAY = 31;

    protected static final int BASE_TIME_HOUR = 2;

    protected static final int BASE_TIME_MINUTE = 2;

    protected static final int BASE_TIME_SECOND = 88;

    protected static final int ALIEN_TIME_MINUTE_IN_SECONDS = 90;

    protected static final int ALIEN_TIME_HOUR_IN_MINUTES = 90;

    protected static final int ALIEN_TIME_DAY_IN_HOURS = 36;

    protected static final long ALIEN_MINUTE_IN_SECONDS = 90;
    protected static final long ALIEN_HOUR_IN_SECONDS = 90 * ALIEN_MINUTE_IN_SECONDS;

    protected static final long ALIEN_DAY_IN_SECONDS = 36 * ALIEN_HOUR_IN_SECONDS;

    protected static int ALIEN_MONTHS_PER_YEAR = 18;

    protected static final int[] ARR_ALIEN_MONTH_DAYS = {44, 42, 48, 40, 48, 44, 40, 44, 42, 40, 40, 42, 44, 48, 42, 40, 44, 38};

    private Long inputYear = null;
    private Long inputMonth = null;
    private Long inputDay = null;
    private Long inputHours = null;
    private Long inputMinutes = null;
    private Long inputSeconds = null;

    private static int getAlienYearTotalDays() {
        int totalAlienDaysPerYear = 0;

        for (int i = 0; i < AlienTime.ARR_ALIEN_MONTH_DAYS.length; i++) {
            totalAlienDaysPerYear += AlienTime.ARR_ALIEN_MONTH_DAYS[i];
        }
//        System.out.println("Total Alien Days per Year: " + totalAlienDaysPerYear);

        return totalAlienDaysPerYear;
    }

    private long getAlienCurrentSeconds() {
        return (System.currentTimeMillis() / 1000) * EarthTime.EARTH_TO_ALIEN_SECONDS_RATIO;
    }

    private long getUnformattedYear() {
        //year & month
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienTotalDays = ALIEN_CURRENT_SECONDS / ALIEN_DAY_IN_SECONDS;
        int alienMonth = 1;
        int alienYear = 0;
        int count = 0;
//        System.out.println("Alien total Days: " + alienTotalDays);
        while (true) {
            if (count > (ALIEN_MONTHS_PER_YEAR - 1)) {
                count = 0;
            }

            if (alienTotalDays > AlienTime.ARR_ALIEN_MONTH_DAYS[count]) {
                alienTotalDays -= AlienTime.ARR_ALIEN_MONTH_DAYS[count];
                alienMonth++;
//                System.out.println("Month_inside: " + alienMonth);
            } else {
                break;
            }

            //if month more than 18 reset back to 1
            if (alienMonth > ALIEN_MONTHS_PER_YEAR) {
                alienMonth = 1;
                alienYear++;
//                System.out.println("Year_inside: " + alienYear);
            }

//            System.out.println("Count: " + count);
            count++;
        }

        alienYear += BASE_TIME_YEAR;

//        System.out.println("Alien Year: " + alienYear);
//        System.out.println("Alien Month: " + alienMonth);

        return alienYear;
    }

    private long getUnformattedMonth() {
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienTotalDays = ALIEN_CURRENT_SECONDS / ALIEN_DAY_IN_SECONDS;
        int alienMonth = 1;
        int alienYear = 0;
        int count = 0;
//        System.out.println("Alien total Days: " + alienTotalDays);
        while (true) {
            if (count > (ALIEN_MONTHS_PER_YEAR - 1)) {
                count = 0;
            }

            if (alienTotalDays > AlienTime.ARR_ALIEN_MONTH_DAYS[count]) {
                alienTotalDays -= AlienTime.ARR_ALIEN_MONTH_DAYS[count];
                alienMonth++;
            } else {
                break;
            }

            //if month more than 18 reset back to 1
            if (alienMonth > ALIEN_MONTHS_PER_YEAR) {
                alienMonth = 1;
                alienYear++;
            }

            count++;
        }

        alienMonth += BASE_TIME_MONTH;

        return alienMonth;
    }

    protected long getUnformattedDay() {
        //day
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienCurrentDaysInSeconds = ALIEN_CURRENT_SECONDS / ALIEN_DAY_IN_SECONDS;
//        System.out.println("Alien Total Days: " + alienCurrentDaysInSeconds);
        long alienDay = alienCurrentDaysInSeconds % getAlienYearTotalDays();
//        System.out.println("Alien day: " + alienDay);

        alienDay += BASE_TIME_DAY;
        return alienDay;
    }

    protected long getUnformattedHour() {
        //hour
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienTotalHours = ALIEN_CURRENT_SECONDS / ALIEN_HOUR_IN_SECONDS;
//        System.out.println("Alien Total Hours: " + alienTotalHours);
        long alienHours = alienTotalHours % 36;
//        System.out.println("Alien Hours: " + alienHours);

        alienHours += BASE_TIME_HOUR;

        return alienHours;
    }

    protected long getUnformattedMinute() {
        //minute
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienTotalMinutes = ALIEN_CURRENT_SECONDS / ALIEN_MINUTE_IN_SECONDS;
//        System.out.println("Alien Total Minutes: " + alienTotalMinutes);
        long alienMinutes = alienTotalMinutes % 90;
//        System.out.println("Alien Minutes: " + alienMinutes);

        alienMinutes += BASE_TIME_MINUTE;

        return alienMinutes;
    }

    protected long getUnformattedSecond() {
        //seconds
        long ALIEN_CURRENT_SECONDS = getAlienCurrentSeconds();
        long alienSeconds = ALIEN_CURRENT_SECONDS % 90;
//        System.out.println("Alien Seconds: " + alienSeconds);

        alienSeconds += BASE_TIME_SECOND;

        return alienSeconds;
    }

    private String checkDigits(String choice, long digit) {
        switch (choice) {
            case "H":
            case "S":
            case "M":
            case "MO":
            case "D":
                return (String.valueOf(digit).length() < 2) ? ("0" + digit) : Long.toString(digit);
            default:
                return "NULL";
        }
    }

    @Override
    public String getFormattedTime(Long year, Long month, Long day, Long hour, Long minute, Long second) {
        if (year == null && month == null && day == null && hour == null && minute == null && second == null) {
            year = getUnformattedYear();
            month = getUnformattedMonth();
            day = getUnformattedDay();
            hour = getUnformattedHour();
            minute = getUnformattedMinute();
            second = getUnformattedSecond();

            if (second >= 90) {
                second -= 90;
                minute++;
            }

            if (minute >= 90) {
                minute -= 90;
                hour++;
            }

            if (hour >= 36) {
                hour -= 36;
                day++;
            }

            if (month > 18) {
                month -= 18;
                year++;
            }

            if (day > ARR_ALIEN_MONTH_DAYS[(Math.toIntExact(month))]) {
                day -= ARR_ALIEN_MONTH_DAYS[(Math.toIntExact(month))];
                month++;
                if (month > 18) {
                    month -= 18;
                    year++;
                }
            }
            return (year + "-" + checkDigits("MO", month) + "-" + checkDigits("D", day) + ", " + checkDigits("H", hour) + ":" + checkDigits("M", minute) + ":" + checkDigits("S", second));
        } else {
            setInputYear(year);
            setInputMonth(month);
            setInputDay(day);
            setInputHours(hour);
            setInputMinutes(minute);
            setInputSeconds(second);

            return (year + "-" + checkDigits("MO", month) + "-" + checkDigits("D", day) + ", " + checkDigits("H", hour) + ":" + checkDigits("M", minute) + ":" + checkDigits("S", second));
        }
    }

    public Long checkYearInput(Long number, Scanner scanner) {
        boolean validNumber = false;

        System.out.print("Enter Alien Year (Min. 2048): ");
        while (!validNumber) {
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 & number < 2048) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Enter year with minimum 2048 year.");
                    System.out.print("Enter Alien Year (Min. 2048): ");
                }
            } else {
                System.out.println("Error: Invalid input. Enter year with minimum 2048 year.");
                System.out.print("Enter Alien Year (Min. 2048): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public Long checkMonthInput(Long number, Scanner scanner) {
        boolean validNumber = false;

        System.out.print("Enter Alien Month (Max: 18): ");
        while (!validNumber) {
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 && number < 19) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Please enter positive value less than 19.");
                    System.out.print("Enter Alien Month (Max: 18): ");
                }
            } else {
                System.out.println("Error: Invalid input. Please enter positive value less than 19.");
                System.out.print("Enter Alien Month (Max: 18): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public Long checkDayInput(Long number, Scanner scanner, Long month) {
        boolean validNumber = false;

        System.out.print("Enter Alien Day (Max: " + Arrays.toString(new int[]{AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)]}) + " Days): ");
        while (!validNumber) {
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 && number <= ARR_ALIEN_MONTH_DAYS[(Math.toIntExact(month) - 1)]) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Max: " + ARR_ALIEN_MONTH_DAYS[(Math.toIntExact(month) - 1)] + " days for Month " + month + ".");                    System.out.print("Enter Alien Day (Max: " + Arrays.toString(new int[]{AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)]}) + " Days): ");
                    System.out.print("Enter Alien Day (Max: " + Arrays.toString(new int[]{AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)]}) + " Days): ");
                }
            } else {
                System.out.println("Error: Invalid input. Max: " + ARR_ALIEN_MONTH_DAYS[(Math.toIntExact(month) - 1)] + " days for Month " + month + ".");
                System.out.print("Enter Alien Day (Max: " + Arrays.toString(new int[]{AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)]}) + " Days): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public Long checkHourInput(Long number, Scanner scanner) {
        boolean validNumber = false;
        System.out.print("Enter Alien Hour (Max: 36): ");
        while (!validNumber) {
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 && number <= 36) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Enter positive value less than 37.");
                    System.out.print("Enter Alien Hour (Max: 36): ");
                }
            } else {
                System.out.println("Error: Invalid input. Enter positive value less than 37.");
                System.out.print("Enter Alien Hour (Max: 36): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public Long checkMinute(Long number, Scanner scanner) {
        boolean validNumber = false;
        while (!validNumber) {
            System.out.print("Please enter Minute (Max: 90): ");
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 && number <= 90) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Enter positive value less than 91.");
                    System.out.print("Please enter Minute (Max: 90): ");
                }
            } else {
                System.out.println("Error: Invalid input. Enter positive value less than 91.");
                System.out.print("Please enter Minute (Max: 90): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public Long checkSecond(Long number, Scanner scanner) {
        boolean validNumber = false;
        System.out.print("Enter Second (Max: 90): ");
        while (!validNumber) {
            if (scanner.hasNextLong()) {
                number = scanner.nextLong();
                if (number > 0 && number <= 90) {
                    validNumber = true;
                } else {
                    System.out.println("Error: Invalid input. Enter a valid number.");
                    System.out.print("Please enter Second (Max: 90): ");
                    scanner.next(); // Clear the invalid input
                }
            } else {
                System.out.println("Error: Invalid input. Enter a valid number.");
                System.out.print("Please enter Second (Max: 90): ");
                scanner.next(); // Clear the invalid input
            }
        }
        return number;
    }

    public String convertToEarthTime() {
        try {
            long year = getInputYear() - BASE_TIME_YEAR;
            long month = getInputMonth() - BASE_TIME_MONTH;
            long day = getInputDay() - BASE_TIME_DAY;
            long hour = getInputHours() - BASE_TIME_HOUR;
            long minute = getInputMinutes() - BASE_TIME_MINUTE;
            long second = getInputSeconds() - BASE_TIME_SECOND;

            if (month < 0) {
                month += 18;
                year--;
            }

            if (day < 0) {
                day += AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)];
                month--;

                if (month == 0) {
                    month = 18;
                }
            }

            if (hour < 0) {
                hour += 36;
                day--;

                if (day == 0) {
                    month--;
                    day += AlienTime.ARR_ALIEN_MONTH_DAYS[(int) (month - 1)];
                }
            }

            if (minute < 0) {
                minute += 90;
                hour--;

                if (hour < 0) {
                    day--;
                    hour += 36;
                }
            }

            if (second < 0) {
                second += 90;
                minute--;

                if (minute < 0) {
                    hour--;
                    minute += 90;
                }
            }

            long yearMillis = year * getAlienYearTotalDays() * ALIEN_TIME_DAY_IN_HOURS * ALIEN_TIME_HOUR_IN_MINUTES * ALIEN_TIME_MINUTE_IN_SECONDS * 1000;
            long monthMillis = getMonthMilis(month);
            long dayMillis = day * ALIEN_TIME_DAY_IN_HOURS * ALIEN_TIME_HOUR_IN_MINUTES * ALIEN_TIME_MINUTE_IN_SECONDS * 1000;
            long hourMillis = hour * ALIEN_TIME_HOUR_IN_MINUTES * ALIEN_TIME_MINUTE_IN_SECONDS * 1000;
            long minuteMillis = minute * ALIEN_TIME_MINUTE_IN_SECONDS * 1000;
            long secondMillis = second * 1000;

            EarthTime et = new EarthTime();

            long alienTotalMilis = yearMillis + monthMillis + dayMillis + hourMillis + minuteMillis + secondMillis;
            long earthTotalMilis = alienTotalMilis / EarthTime.EARTH_TO_ALIEN_SECONDS_RATIO;

            System.out.println("Alien Time: " + getFormattedTime(getInputYear(), getInputMonth(), getInputDay(), getInputHours(), getInputMinutes(), getInputSeconds()));
            return et.getConvertedAlienToEarthTime(earthTotalMilis);

        } catch (NullPointerException e) {
            return "Don't have saved date and time information, please run menu item 2. to save date and time";
        }

    }

    private Long getMonthMilis(Long month) {
        Long monthMilis = 0L;
        int totalDays = 0;

        month -= 2; //reduce 1 for index 1 for previous month

        for (int i = 1; i <= month; i++) {
            totalDays += ARR_ALIEN_MONTH_DAYS[(int) (month - 2)];
        }

        monthMilis = (long) totalDays * ALIEN_TIME_DAY_IN_HOURS * ALIEN_TIME_HOUR_IN_MINUTES * ALIEN_TIME_MINUTE_IN_SECONDS * 1000;

        return monthMilis;
    }

    public long getInputYear() {
        return inputYear;
    }

    public void setInputYear(long inputYear) {
        this.inputYear = inputYear;
    }

    public long getInputMonth() {
        return inputMonth;
    }

    public void setInputMonth(long inputMonth) {
        this.inputMonth = inputMonth;
    }

    public long getInputDay() {
        return inputDay;
    }

    public void setInputDay(long inputDay) {
        this.inputDay = inputDay;
    }

    public long getInputHours() {
        return inputHours;
    }

    public void setInputHours(long inputHours) {
        this.inputHours = inputHours;
    }

    public long getInputMinutes() {
        return inputMinutes;
    }

    public void setInputMinutes(long inputMinutes) {
        this.inputMinutes = inputMinutes;
    }

    public long getInputSeconds() {
        return inputSeconds;
    }

    public void setInputSeconds(long inputSeconds) {
        this.inputSeconds = inputSeconds;
    }
}
