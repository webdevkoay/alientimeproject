package AlienTime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EarthTime implements  TimeInterface{

    protected static final int EARTH_TO_ALIEN_SECONDS_RATIO = 2;

    @Override
    public String getFormattedTime(Long year, Long month, Long day, Long hour, Long minute, Long second) {
        long earthTimeMillis = System.currentTimeMillis();
        // Convert Earth time to desired format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date earthTime = new Date(earthTimeMillis);


        return sdf.format(earthTime);
    }

    public String getConvertedAlienToEarthTime(long earthMillis){
        // Convert Earth time to desired format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date earthTime = new Date(earthMillis);


        return sdf.format(earthTime);
    }
}
