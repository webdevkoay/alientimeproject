package AlienTime;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        AlienTime alien = new AlienTime();
        EarthTime earth = new EarthTime();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the Alien AlienTime.Clock Program!");
        while (true) {
            System.out.println("\nMenu:");
            System.out.println("1. Display Alien clock based on current Earth date and time");
            System.out.println("2. Convert saved alien datetime to Earth datetime");
            System.out.println("3. Exit");

            System.out.print("\nEnter your choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1 -> {
                    runCase1(scanner, alien, earth);
                }
                case 2 -> {
                    runCase2(scanner);
                }
                case 3 -> {
                    System.out.println("Exiting the program...");
                    return;
                }
                default -> System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    protected static void runCase1(Scanner scanner, AlienTime alien, EarthTime earth) throws InterruptedException {
        int count = 0;
        boolean continueDisplay = true;
        do {
            System.out.println("Earth current time is " + earth.getFormattedTime(null, null, null, null, null, null));
            System.out.println("Alien current time is " + alien.getFormattedTime(null, null, null, null, null, null));
            Thread.sleep(500);

            if (++count % 10 == 0) {
                System.out.print("Would you like to continue? (Y/N): ");
                String input = scanner.next();

                if (input.equalsIgnoreCase("N")) {
                    continueDisplay = false;
                }
            }
        } while (continueDisplay);
    }

    protected static void runCase2(Scanner scanner){
        Long year = 0L;
        Long month = 0L;
        Long day = 0L;
        Long hour = 0L;
        Long minute = 0L;
        Long second = 0L;

        AlienTime alien = new AlienTime();

        year = alien.checkYearInput(year, scanner);
        month = alien.checkMonthInput(month, scanner);
        day = alien.checkDayInput(day, scanner, month);
        hour = alien.checkHourInput(hour, scanner);
        minute = alien.checkMinute(minute, scanner);
        second = alien.checkSecond(second, scanner);
        String time = alien.getFormattedTime(year, month, day, hour, minute, second);
        String result = alien.convertToEarthTime();


        System.out.println("Saved Alien Time: " + time);
        System.out.println("Convert To Earth Time: " + result);
    }
}