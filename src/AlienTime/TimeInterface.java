package AlienTime;

public interface TimeInterface {

    public String getFormattedTime(Long year, Long month, Long day, Long hour, Long minute, Long second);
}
