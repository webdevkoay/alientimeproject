package CardGame;

import java.util.*;

class Card {
    private String alphanumeric;
    private char symbol;

    public Card(String alphanumeric, char symbol) {
        this.alphanumeric = alphanumeric;
        this.symbol = symbol;
    }

    protected static List<Card> generateDeck() {
        List<Card> deck = new ArrayList<>();
        String[] alphanumeric = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        char[] symbols = {'@', '#', '^', '*'};

        for (String alpha : alphanumeric) {
            for (char symbol : symbols) {
                deck.add(new Card(alpha, symbol));
            }
        }

        return deck;
    }

    protected static void displayCards(List<Card> cards) {
        for (Card card : cards) {
            System.out.print(card + " ");
        }
        System.out.println();
    }

    protected static List<List<Card>> distributeCards(List<Card> deck, int numPlayers) {
        List<List<Card>> players = new ArrayList<>();

        for (int i = 0; i < numPlayers; i++) {
            players.add(new ArrayList<>());
        }

        int playerIndex = 0;
        for (Card card : deck) {
            players.get(playerIndex).add(card);
            playerIndex = (playerIndex + 1) % numPlayers;
        }

        return players;
    }

    protected static Map<Integer, List<Card>> findWinner(List<List<Card>> players) {
        Map<Integer, List<Card>> winners = new HashMap<>();
        int maxCount = 0;

        for (int i = 0; i < players.size(); i++) {
            List<Card> hand = players.get(i);
            Map<String, Integer> countMap = new HashMap<>();
            int maxPairCount = 0;

            for (Card card : hand) {
                String alphanumeric = card.getAlphanumeric();
                int count = countMap.getOrDefault(alphanumeric, 0) + 1;
                countMap.put(alphanumeric, count);
                maxPairCount = Math.max(maxPairCount, count);
            }

            if (maxPairCount > maxCount) {
                maxCount = maxPairCount;
                winners.clear();
                winners.put(i, hand);
            } else if (maxPairCount == maxCount) {
                winners.put(i, hand);
            }
        }

        return winners;
    }

    public String getAlphanumeric() {
        return alphanumeric;
    }

    public char getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return alphanumeric + symbol;
    }
}

