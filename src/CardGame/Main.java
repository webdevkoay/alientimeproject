package CardGame;

import java.util.*;

class Main {
    public static void main(String[] args) {
        System.out.println("How many games: ");
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        for (int x = 0; x < count; x++) {
            System.out.println("=============================Game " + (x + 1) + " Begin=============================");
            List<Card> deck = Card.generateDeck();
            System.out.println("Deck before shuffling:");
            Card.displayCards(deck);

            Collections.shuffle(deck);
            System.out.println("\nDeck after shuffling:");
            Card.displayCards(deck);

            List<List<Card>> players = Card.distributeCards(deck, 4);
            System.out.println("\nPlayer hands:");
            for (int i = 0; i < players.size(); i++) {
                System.out.println("Player " + (i + 1) + ": " + players.get(i));
            }

            Map<Integer, List<Card>> winner = Card.findWinner(players);
            System.out.println("\nWinner: Player " + (winner.keySet().iterator().next() + 1));
            System.out.println("Player " + (winner.keySet().iterator().next() + 1) + "'s hand: " + winner.values().iterator().next());
            System.out.println("=============================Game " + (x + 1) + " End===============================");
            System.out.println("");
        }
    }
}
